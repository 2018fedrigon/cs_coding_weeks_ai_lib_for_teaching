# Objectif 2 : créeons un projet open source !


L'objectif de ce projet est de developper en open-source (et donc il faudra bien respecter les consignes du [tutorial](https://opensource.guide/fr/how-to-contribute/).


L'objectif de ce projet est de développer une bibliothèque permettant pour un enseignant en IA, de concevoir et de mettre en oeuvre rapidement des challenges d'IA autour du jeu.

Nous vous partageons [ici](https://gitlab.com/hudelotc/DTY_AI_WEEK_2018/tree/master/Day2/Lab) l'exemple d'un tel challenge qui est proposé aux étudiants de la Digital Tech Year autour du puissance 4.

Vous avez aussi travailler de votre côté du le jeu 2048  et nous pourrions par exemple vous demander de faire un IA pour joueur à ce jeu.

L'idée ici est de pouvoir faire une sorte d'aide à la conception de ce type de jeux et de challenges. L'utilisateur rentrerait par exemple des paramètres de type :

 + Taille et forme de la grille de jeu.
 + Nombre de joueurs
 + Règles du jeu
 + ...
et la bibliothèque proposé pourrait permettre de générer ces jeux.


Si vous pensez ne pas avoir assez terminé le jeu 2048, alors vous pouvez pendre le temps de terminer au moins jusqu'à l'objectif 3. 

**A VOUS DE JOUER** !  A ce stade, nouss vous laissons vous même faire les étapes de découpages en Sprint et fonctionnalité.



